# Working with Exceptions in ruby.. 
print("\nProgram Starts\n\n-------------------------------------------------------------------------- \n")
# Module 1

print("\n\n----------------------------------\n\nRaising, Handling an exception - Module 1 Output\n\n----------------------------------\n\n")

def inverse(x)
	begin
		raise "Argument is not numeric" unless x.is_a? Numeric
	return	1.0/x
	rescue
		puts("The inverse method doesn't take a string as a formal argument when called")
	end
		puts("\nFinally out of the begin-end block")
end

print("Output, if the inverse method called has a number passed with the call: ")
y = inverse(2)
puts(y)
print("\nOutput, if the inverse method called has a String passed with the call: ")
y = inverse(' hi!! ')
puts(y)

# Module 2

print("\n\n----------------------------------\n\nRerunning after an exception - Module 2 Output\n\n----------------------------------\n\n")

def rescue_and_retry
	error_fixed = false
	begin
		puts 'I am before the raise in the begin block'
	raise 'An error has occured!' unless error_fixed
		puts 'I am after the raise in the begin block.'
	rescue
		puts 'An exception was thrown! Retrying...'
		error_fixed = true
		retry
	end
	puts 'I am after the begin block'
end

rescue_and_retry
	
		
print("\nProgram Ends\n\n---------------------------------------------------------------------------- \n\n")

