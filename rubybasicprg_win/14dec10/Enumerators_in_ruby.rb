# Working with Enumerators
print("\nProgram Starts\n\n-------------------------------------------------------------------------- \n")

# Module 1

print("\n\n----------------------------------\n\nWonders with Enumerators - Module 1 Output\n\n----------------------------------\n\n")

enumerator = 3.times
puts("\n")
enumerator.each { |x| print x }
puts("\n\n")


10.downto(1).select { |x| puts x if x%2 == 0 }
puts("\n")
puts("hello".each_byte.to_a)
puts("\n")
(1..10).each_slice(3) {|a| p a}
puts("\n")
(1..10).each_cons(3) {|a| p a}
puts("\n")

