# A sample Hello world program in Ruby with user input for a string and the same being printed
print("\nProgram Starts\n\n-------------------------------------------------------------------------- \n")

#----------------------------------------------------------------------------------------------------------------------------------------------------

#Module 1
print("\nModule 1 Output\n\n-------------------------------------------------------------------- \n")

puts("Hello World")

#----------------------------------------------------------------------------------------------------------------------------------------------------

#Module 2
print("\nModule 2 Output\n\n-------------------------------------------------------------------- \n") 

print('HI..!! enter ur name please: ')

#Input user name through gets()

name = gets()
puts("Hello #{name}")

#this symbol is mainly used for comments in Ruby..

puts("\n")

print("\nProgram Ends\n\n---------------------------------------------------------------------------- \n\n")
