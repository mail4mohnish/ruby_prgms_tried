# Conditionals
print("\nProgram Starts\n\n-------------------------------------------------------------------------- \n")

# Module 1
print("\nModule 1 Output\n\n-------------------------------------------------------------------- \n")

a = 5
b = 3

# if else in operation..

if a < b
	puts("true")
else
	puts("false")
end


# if elsif else in operation..

if a < b
	puts("a is less than b")
elsif a == b
	puts("a is equal to b")
else
	puts("a is greater than b")
end


# if that returns a value

x = 2
month = if x == 1 then "january"
	elsif x == 2 then "february"
	elsif x == 3 then "march"
	elsif x == 4 then "april"
	else	"The Month is not either of the above four months"
	end

puts(month)

# a different Avatar of if.. Ruby making things simpler..

puts x if x # if x is defined it will print the value of x.. do note..


#--------------------------------------------------------------------------------------------------------------------------------

# Module 2
print("\nModule 2 Output\n\n-------------------------------------------------------------------- \n")


# unless

unless a == 3 # this condition is false or nil.. the actual value of a is 5.. this condition works as the opposite of the if condition..
	puts("value of a is not 3 ")
end


# unless with else

unless x == 0
	puts "x is not 0"
	else
	unless y == 0
		puts "y is not 0"
		else
		unless z == 0
			puts "z is not 0"
			else
			puts " all are zero "
		end
	end
end	
#--------------------------------------------------------------------------------------------------------------------------------

# Module 3
print("\nModule 3 Output\n\n-------------------------------------------------------------------- \n")

# case

name =  case
		when x == 1 then "one"
		when 2 == x then "two"
		when x == 3 then "three"
		when x == 4 then "four"
		when x == 5 then "five"
		else "nothing among the first five"
	end
puts(name)

print("\nProgram Ends\n\n---------------------------------------------------------------------------- \n\n")
#--------------------------------------------------------------------------------------------------------------------------------


