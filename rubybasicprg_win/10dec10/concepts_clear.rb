a = 0

if (a) #imp learning
	puts("It enters the if condn with a zero value")
	#entering here means that a is defined, else not
else
	puts("It doesn't enter if condn. wit a zero value")
end

puts("\n")

b = nil

if (b)
	puts("It enters the if condn with a nil value")
else
	puts("It doesn't enter if condn. wit a nil value")
end

puts("\n")

c = false

if (c)
	puts("It enters the if condn with a false value")
else
	puts("It doesn't enter if condn. wit a false value")
end

d = 13
e = 7

t1 = a && d
puts(t1)

t2 = a & d
puts(t2)


t3 = d && a
puts(t3)

t4 =  d & a
puts(t4)

print("\n now checking wrt false n nil\n")

t5 = d && nil
puts("value of t5: #{t5}")

#t6 =  d & false  similarly wit false.. as wrt nil given below..
#puts(t6)

t7 = nil && d
puts("value of t7: #{t7}")

#t7 = d & nil , you can't perform a bitwise operation with a nil.., this is an error hence its commented..
#puts(t7)

t6 = nil & d
puts(t6)


#t8 =  false & d
#puts(t8)

t9 = d & e
puts(t9)



t10 = d && e # where exactly this is used, try to come back Mohnish
puts(t10)


t11 = e && d
puts(t11)


t12 = e & d
puts(t12)


# finally the & we can make out performs an 'AND' operation BITWISE... for e.g. here.. d = 13 which 1101 in binary, e = 7 which is 0111 in binary..
# now we are doing d&e so we get finally 0101... which if 5, hence the op
