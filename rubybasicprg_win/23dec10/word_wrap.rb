str = "\
I don't necessarily need code examples -- but if anyone has
ideas for a best approach to specifying a line wrap width
(breaking between words for lines no longer than a specific
column width) for output from a Ruby script, I'd love to
hear about it."

X = 40
puts str.gsub(/\n/," ").scan(/\S.{0,#{X-2}}\S(?=\s|$)|\S+/)



