# Working with Exceptions in ruby.. 
print("\nProgram Starts\n\n-------------------------------------------------------------------------- \n")
# Module 1

print("\n\n----------------------------------\n\nRaising, Handling an exception - Module 1 Output\n\n----------------------------------\n\n")

def inverse(x)
	begin
		raise "Argument is surely not numeric" unless x.is_a? Numeric
	return	1.0/x
	rescue Exception => e
		puts("The inverse method doesn't take a string as a formal argument when called, \nThe error raised is: ")
		puts e.message
		# This gives the name of the message that would have otherwise been called through raise, if there was no rescue
		puts("The line with respect which the error was raised is: ")
		puts e.backtrace.inspect
	else
		puts 'How are you Sir'
	ensure
		puts "\nThe ensure clause is surely always executed"
	end
		puts("\nFinally out of the begin-end block")
#This looks to be more like a finally block of java
end

print("Output, if the inverse method called has a number passed with the call: ")
y = inverse(2)
puts(y)
print("\nOutput, if the inverse method called has a String passed with the call: ")
y = inverse(' hi!! ')
puts(y)

# Module 2

print("\n\n----------------------------------\n\nRerunning after an exception - Module 2 Output\n\n----------------------------------\n\n")

def rescue_and_retry
	error_fixed = false
	begin
		puts 'I am before the raise in the begin block'
		raise 'An error has occured!' unless error_fixed
		puts 'I am after the raise in the begin block.'
	rescue
		puts 'An exception was thrown! Retrying...'
		error_fixed = true
		retry
	else
		puts 'How are you Sir'
	end
	puts 'I am after the begin block'
end

rescue_and_retry

# Module 3

print("\n\n----------------------------------\n\nPropagating an exception - Module 3 Output\n\n----------------------------------\n\n")


def explode
 raise "bam"  if ( (a = rand(10)) == 0 )
 puts("Value of a is: #{a}")
# do note the value of 'a' will be printed along with "hello" if a is non-zero throughout total number of times the  explode method is called, else not.
end

#count = 0

def risky
	begin
		5.times do
				#count = count + 1
				#puts("Calling explode for #{count} time")
				explode
			end
	rescue TypeError
		puts $!
	# The global variable $! refers to the Exception object that is being handled
	end
	puts "hello"
end


def defuse
	begin
		puts risky
	rescue RuntimeError => e
		puts e.message
	end
end

defuse

#note the way through which a method can be called in Ruby

print("\n")
puts(rand(10))
#cool thing.. check the working of the rand function

# Module 4

print("\n\n----------------------------------\n\nThe working of throw and catch - Module 4 Output\n\n----------------------------------\n\n")


def getValue(field)
	print field
	res = readline.chomp
	#reads a line from the console
	throw :quitDataEntry if res == '!'
	return res
end

catch :quitDataEntry do
	name = getValue("Name: ")
	eid = getValue("Employee_id: ")
	if(name)
		puts name
	end
end

		
print("\nProgram Ends\n\n---------------------------------------------------------------------------- \n\n")

