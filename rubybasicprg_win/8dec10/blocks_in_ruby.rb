1.upto(10) { |x| puts x }

1.upto(10) do |x|
	puts x
end

1.upto(3) { |x| puts x }
#1.upto(10)
#{ |x| puts x }

week = Hash.new
week = { :sunday => 1, :monday => 2, :"tuesday" => 3, :wednesday => 4, :thursday => 5, :'friday' => 6, :saturday => 7 }

week.each do | key, value |
	puts "#{key}: #{value}"
end

