 #blk type 1

1.upto(4).each {|i| p "Hello #{i}"} 



#blk type 2
[1,4].each do |i| puts "h #{i}" end



#blk type 3 eg
def foo(x)
  puts "OK: called as foo(#{x.inspect})"
  yield("A gift from foo!") if block_given?
end

#blk type 4 eg
foo(10)

foo(123) {|y| puts "BLOCK: #{y} How nice =)"}

foo(124) {|a| puts "blk- #{a}, printing it? - Yep:)"}

 def add_lambda(a,b,operation)
 	operation.call(a,b)
 end

 def add_via_yield(a,b)
 	yield(a,b) #it implicitly does the specific operation
 end

p add_lambda(4,5, lambda {|a,b| a+b})

p add_via_yield(4,52) {|a, b| a+b} #in this case the blk is not passed as a parameter..


module Dynamismruby

  def hello_from_dynamism
    p "hello from dynamism in ruby"
  end
  
	def msg_passin
	  puts "Hi, which method do you like to invoke on a string today?"
	  method_name = gets.strip
	  puts "a random string".send(method_name)
	end

	def exec_code_at_runtime
	  puts "What code do you want to run today, dear sir? - Pls press ctrl+d for stop input\n Sample code to run - 1.upto(5) {|x| p x*x}"
	  arbitrary_code = STDIN.read         # press ctrl+d to stop input
	  eval(arbitrary_code)	 
	end
end

# sample code to run - 1.upto(5) {|x| p x*x}


# ref - https://rubymonk.com/learning/books/4-ruby-primer-ascent/chapters/18-blocks/lessons/51-new-lesson
class TestMarks
  include Dynamismruby
  def tot_marks(a,b)
  	#tot = a+b
  	return a+b
  end


end  

ab = TestMarks.new.method("tot_marks")
p "*******\nModules in TestMarks class - #{TestMarks.included_modules}*****\n"
#Dynamismruby.msg_passin
tst_obj = TestMarks.new
#tst_obj.msg_passin
#ab.msg_passin
#tst_obj.exec_code_at_runtime - commeting wrt rspec for now.. 
blk_tot_marks = ab.to_proc
p blk_tot_marks.call(49,32)