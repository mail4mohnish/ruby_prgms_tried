#Being able to write some_function(lambda { |x,y| x + f(y) }) is much nicer than having to write
# Just like these features, anonymous functions are there to help you express things elegantly, concisely, and sensibly.

a1 = lambda { |x,y| return x + f(y) }


def f(a)
	a += 2
	return a
end


def temp(x,y)
	s1 = x + f(y)
	puts s1
end

s2 = a1.call(2,3)
puts s2

temp(2,9)


