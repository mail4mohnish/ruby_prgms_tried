
# working of return, redo, break, next
# do note retry does not work in Ruby 1.9 onwards, thus its not included here

print("\nProgram Starts\n\n-------------------------------------------------------------------------- \n")


#--------------------------------------------------------------------------------------------------------------------------------

# return

# Module 1
print("\nModule 1 Output\n\n-------------------------------------------------------------------- \n")


print("Working of return: ")

print("enter the number whose factorial you want to get: ")
n = gets.to_i


def factorial(n)
	raise "bad argument" if n<1
	return 1 if n == 1
	n * factorial(n-1)
end

x = factorial(n) 
puts(x)

# even here its important to note the factorial method will be called only after its defined and not otherwise

#--------------------------------------------------------------------------------------------------------------------------------

# break

# Module 2
print("\nModule 2 Output\n\n-------------------------------------------------------------------- \n")


print("Working of break: ")

x = 10
while x >= 0 do
	break if x == 5 
	puts x
	x = x - 1
end
#--------------------------------------------------------------------------------------------------------------------------------

# next

# Module 3
print("\nModule 3 Output\n\n-------------------------------------------------------------------- \n")

filename = "to_print_contents.txt"

File.open(filename) do |f|
f.each do |line|
	next if line[0,1] == "#" # the next functions like a "continue" in Java or C programming..
	print line # it reads from a file and if it encounters a line with beginning with a comment then it skips that line and prints the next non comment line..
end	
end

# the next functions like a "continue" in Java or C programming..
# it reads from a file and if it encounters a line with beginning with a comment then it skips that line and prints the next non comment line..


#--------------------------------------------------------------------------------------------------------------------------------

# redo

# Module 4
print("\nModule 4 Output\n\n-------------------------------------------------------------------- \n")


puts "\nPlease enter the first word you think of when you come across the following: "
words = %w( apple banana cherry )
response = words.collect do |word|
	print word + "> "
	response = gets.chop
	if response.size == 0
		word.upcase! 
		redo
	end     
	response
end

# two things to note here... if doesn't require the parentheses we use otherwise..
# also if we don't enter an appropriate string we think of while encountering apple etc. through redo we are prompted again to
# to choose an appropriate string.. but this time we would be choosing for 'APPLE' and not 'apple' do note..
# this prompting the user that the string equivalent you think of can't be left blank my reitrating through the same word in CAPS..

print("\nProgram Ends\n\n---------------------------------------------------------------------------- \n\n")
#--------------------------------------------------------------------------------------------------------------------------------




