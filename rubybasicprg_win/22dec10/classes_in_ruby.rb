# Working with Classes
print("\nProgram Starts\n\n-------------------------------------------------------------------------- \n")

# Module 1
print("\n\n----------------------------------\n\nClasses in Ruby(Basics) - Module 1 Output\n\n----------------------------------\n\n")


class Point

	x = 2
	y = 3
	p = Point.new
	puts p.class
	puts p.is_a? Point
	@x, @y = x,y	
	
end

class Point2
	
	def initialize(x,y)
	@x, @y = x,y
	end

	
end


p2 = Point2.new(1,2)
puts @x, @y


class Point3

	def initialize(x,y)
		@x, @y = x,y
	end

	def x
		@x
	end

	def y
		@y
	end

	def x=(value)
		@x = value
	end

	def y=(value)
		@y = value
	end

end

p3 = Point3.new(9,4)
puts p3.x
puts p3.y
#calling the getter methods for instance variables x & y of Point3 class

print "\n"

p4 = Point3.new(p3.x*2, p3.y*3) 
puts p4.x
puts p4.y

#p5 = Point3.new(1,1)
print "\n"
puts p3
#I think it prints the object reference address( it changes for every run )
print "\n"
p3.x = 0
#calling the setter methods of Point3 class
puts p3.x
#p.y = 0

# Module 2
print("\n\n----------------------------------\n\nDuck Typing - Module 2 Output\n\n----------------------------------\n\n")

class Duck

	def initialize( x, y)
		@x, @y = x,y
	end
	
	attr_reader :x, :y
	#Another form of the accessor methods

	def quack
		'Quack!'
	end
	
	def swim
		'Paddle paddle paddle...'
	end

	def +(other)
		Duck.new(@x + other.x, @y + other.y)
	end
	
end



class Goose
	def honk
		'Honk!'
	end

	def swim
		'Splash splash splash....'
	end
end

class DuckRecording

	def quack
		play
	end

	def play
		'Quack!'
	end

end

p1 = Duck.new(1,2)
puts p1.x
print "\n"

p2 = Duck.new(4,5)

p3 = Duck.new(0,0)

p3 = p1 + p2
puts p3.x
print "\n"

def make_it_quack(duck)
	duck.quack
end


puts make_it_quack(k1 = Duck.new(4,5))
puts k1.x
print "\n"

puts make_it_quack(DuckRecording.new)
print "\n"

def make_it_swim(duck)
	duck.swim
end

puts make_it_swim(Duck.new(3,4))
puts make_it_swim(Goose.new)
print "\n"

puts make_it_quack(Goose.new)
#Check the behavoir for the above statement


# Module 3
print("\n\n----------------------------------\n\nClass Variables and Methods, Constants - Module 3 Output\n\n----------------------------------\n\n")

class Point4
	attr_reader :x, :y

	def Point4.sum(*points)
		x = y = 0
		points.each { |p| x += p.x; y += p.y }
		# The semicolon is like a delimiter.. You need not have every statement in a new line once you use this..
		Point4.new(x,y)
	end

end

p4 = Point4.new(1,3)
p5 = Point4.new(4,23)
p4.sum( p5 )
puts p4.x






