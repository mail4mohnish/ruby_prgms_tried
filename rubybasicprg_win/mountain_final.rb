print("enter the tot no. of levels for traversal: ")

levels = gets.to_i
count_levels = 0
up = 0
down = 0
walk = 0
flag = 0
total_ups = 0
total_downs = 0
trace_steps = []
trace_direction = ['a']
trace_steps_index = 0
trace_directions_index = 0
direction = []
direction_index = 0
total_walk_distance = 0
print_terrain_index = 0

if( levels <= 20 )
	get_newline = gets()

	if get_newline == "\n"
#		print("Successfully executed until here, now go ahead and enter the path of traversal: ")

		while( flag != 1 && count_levels <= levels ) # write now I am only checking for upward movement frm bottom left corner
			direction[direction_index] = gets.chomp
#			puts(direction_index)

#			puts("current input obtained: #{direction[direction_index]} ")

			case 

			when direction[direction_index] == '/' 
			then
#				puts("Entering case: #{direction[direction_index]}")
				#puts("#{direction[direction_index-1]}") # now this is interesting.. remember in ruby 0-1 -> -1.. it takes 
				if( direction[direction_index-1] == '_' ) # n prints the first element from the last and give op..for the first time
					trace_steps[trace_steps_index] = walk # thats the first element only..

					if ( walk > 1 )
						trace_direction[trace_directions_index] = "steps straight"
					else
		                         	trace_direction[trace_directions_index] = "step straight"
					end

					walk = 0 # this has to be done..
					trace_steps_index += 1
					trace_directions_index += 1
#					puts(trace_steps[trace_steps_index])
#					puts(trace_direction[trace_directions_index])
#					puts(trace_steps_index)
#					puts(trace_directions_index)
				elsif ( direction[direction_index-1] == '\\' )
					trace_steps[trace_steps_index] = down

					if ( down > 1 )
						trace_direction[trace_directions_index] = "steps down"
					else
						trace_direction[trace_directions_index] = "step down" 
					end										

					down = 0
					trace_steps_index += 1
					trace_directions_index += 1
#					puts(trace_steps[trace_steps_index])
#					puts(trace_direction[trace_directions_index])
#					puts(trace_steps_index)
#					puts(trace_directions_index)
				else
				end
			
				up += 1
				total_ups += 1
				count_levels += 1 #this has to be done else user might try to go up more levels than previously entered			
				total_walk_distance += 1 # it needs to be checked for...
#				puts("Entering the condition of up: #{total_walk_distance} and ups: #{up}\n")

			when direction[direction_index] == '\\'
			then
#				puts("Entering case: #{direction[direction_index]}")
				if( direction[direction_index-1] == '_' )
					trace_steps[trace_steps_index] = walk

					if ( walk > 1 )
						trace_direction[trace_directions_index] = "steps straight"
					else
		                         	trace_direction[trace_directions_index] = "step straight"
					end
	
					walk = 0
					trace_steps_index += 1
					trace_directions_index += 1
#					puts(trace_steps[trace_steps_index])
#					puts(trace_direction[trace_directions_index])
#					puts(trace_steps_index)
#					puts(trace_directions_index)
				elsif ( direction[direction_index-1] == '/' )
#					puts(up)
					trace_steps[trace_steps_index] = up
			
					if ( up > 1 )
						trace_direction[trace_directions_index] = "steps up"
					else
		                         	trace_direction[trace_directions_index] = "step up"
					end

					up = 0
					trace_steps_index += 1
					trace_directions_index += 1
#					puts("#{trace_steps[trace_steps_index]}")
#					puts("#{trace_direction[trace_directions_index]}")
#					puts(trace_steps_index)
#					puts(trace_directions_index)
				else # do nothing
				end

				down += 1
				total_downs += 1
				count_levels -= 1
				total_walk_distance += 1

				if total_ups == total_downs	#its a balanced traversal...This is for the final stage....
					trace_steps[trace_steps_index] = down

					if ( down > 1 )
						trace_direction[trace_directions_index] = "steps down"
					else
						trace_direction[trace_directions_index] = "step down" 
					end										
					
					trace_steps_index += 1
					trace_directions_index += 1
#					puts(trace_steps[trace_steps_index])
#					puts(trace_direction[trace_directions_index])
#					puts(trace_steps_index)
#					puts(trace_directions_index)
					flag = 1				
				end
#				puts("Entering the condition of down: #{total_walk_distance} and downs: #{down}\n")
		
			when direction[direction_index]== '_'
			then
#				puts("Entering case: #{direction[direction_index]}")
				if( direction[direction_index-1] == '\\' )
					trace_steps[trace_steps_index] = down # it takes the previous value of down if that was the previous step

					if ( down > 1 )
						trace_direction[trace_directions_index] = "steps down"
					else
						trace_direction[trace_directions_index] = "step down" 
					end	# accordingly we try to keep track of the terrain traversed 

					down = 0					
					trace_steps_index += 1 # in all cases
					trace_directions_index += 1
#					puts(trace_steps[trace_steps_index])
#					puts(trace_direction[trace_directions_index])
#					puts(trace_steps_index)
#					puts(trace_directions_index)
				elsif ( direction[direction_index-1] == '/' )
					trace_steps[trace_steps_index] = up
					
					if ( up > 1 )
						trace_direction[trace_directions_index] = "steps up"
					else
		                         	trace_direction[trace_directions_index] = "step up"
					end

					up = 0					
					trace_steps_index += 1
					trace_directions_index += 1
#					puts(trace_steps[trace_steps_index])
#					puts(trace_direction[trace_directions_index])
#					puts(trace_steps_index)
#					puts(trace_directions_index)
				else # just check if execution ends over here..
				end

				walk += 1
		
				total_walk_distance += 1
#				puts("Entering the condition of walks: #{total_walk_distance} and walks: #{walk}\n")

			when direction[direction_index] == " "
			then
# I need to edit this part...... seriously....... also to chck wrt other conditions.. vry imp....

					if( direction[direction_index-1] == '_' )
					trace_steps[trace_steps_index] = walk # thats the first element only..
					if ( walk > 1 )
						trace_direction[trace_directions_index] = "steps straight"
					else
		                         	trace_direction[trace_directions_index] = "step straight"
					end

					walk = 0 # this has to be done..
					trace_steps_index += 1
					trace_directions_index += 1
#					puts(trace_steps[trace_steps_index])
#					puts(trace_direction[trace_directions_index])
#					puts(trace_steps_index)
#					puts(trace_directions_index)
				elsif ( direction[direction_index-1] == '\\' )
					trace_steps[trace_steps_index] = down

					if ( down > 1 )
						trace_direction[trace_directions_index] = "steps down"
					else
						trace_direction[trace_directions_index] = "step down" 
					end										

					down = 0
					trace_steps_index += 1
					trace_directions_index += 1
#					puts(trace_steps[trace_steps_index])
#					puts(trace_direction[trace_directions_index])
#					puts(trace_steps_index)
#					puts(trace_directions_index)
				else
				end
			
				up += 1
				total_ups += 1
				count_levels += 1 #this has to be done else user might try to go up more levels than previously entered			
				total_walk_distance += 1 # it needs to be checked for...
#				puts("Entering the condition of up: #{total_walk_distance} and ups: #{up}\n")


				next # to ignore and continue
			end # end case
			direction_index += 1
		end # end while
	end #end inner if condition
else
	print( "the number of levels in the terrain can't be more than 20 " )
end #end outer if condition


puts("Total walk distance: #{total_walk_distance}")

#puts("\nTracing the path of terrain traversed: \n")
#puts(trace_steps.size)

while ( print_terrain_index != trace_steps.size )
	puts("#{trace_steps[ print_terrain_index ]} #{trace_direction[print_terrain_index]}\n" )
	print_terrain_index += 1
end

#puts("\n directions available: ")

#puts(trace_direction)

#puts("\n appropriate step numbers: ")

#puts(trace_steps)


