#Playin with Hashes..:)
print("\nProgram Starts\n\n-------------------------------------------------------------------------- \n")

# Module 1
print("\nModule 1 Output\n\n-------------------------------------------------------------------- \n")

months = Hash.new

months["january"] = 1
months["july"] = 7

total_months = months["january"] + months["july"]

puts("Total Months via hashes: #{total_months} ")

puts "#{months["june"]}"

#note how a value of null is assigned to a hash of months .. as it was defined with no default initial value

#--------------------------------------------------------------------------------------------------------------------------------
# Module 2
print("\nModule 2 Output\n\n-------------------------------------------------------------------- \n")


#also check how puts is working without the use of parentheses..

numbers = Hash.new{"zero"}

puts '#{numbers["two"]}' 

# givin it this way won't work.. need to use double quotes...

puts "#{numbers["two"]}" 

# this prints the default value of zero for key "two" as no value was given to it


#--------------------------------------------------------------------------------------------------------------------------------

# Module 3
print("\nModule 3 Output\n\n-------------------------------------------------------------------- \n")

# usage of hash literals... as key value pairs.. Here we are using symbols.. but even quotes can be used.. as shown above..

week = Hash.new
week = { :sunday => 1, :monday => 2, :"tuesday" => 3, :wednesday => 4, :thursday => 5, :'fri day' => 6, :saturday => 7 }

puts "#{week[:thursday]}"


print("\nProgram Ends\n\n---------------------------------------------------------------------------- \n\n")
#-------------------------------------------------------------------------------------------------------------------------------- 
