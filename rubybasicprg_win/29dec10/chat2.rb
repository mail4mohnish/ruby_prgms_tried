require 'gserver'

class BasicServer < GServer

  def initialize(*args)
    super(*args)
    
    # Keep an overall record of the client IDs allocated
    # and the lines of chat
    @@client_id = 0
    @@chat = []
  end
  

  def serve(io)

	  #  io.puts("Hello world!")
	  # Increment the client ID so each client gets a unique ID
    @@client_id += 1
    my_client_id = @@client_id
    my_position = @@chat.size
    # the position of the particular person in the chat array... so that it later on can be referenced for future comm. wrt msgs entered by him/her
    io.puts("Welcome to the chat, client #{@@client_id}!\n")
	
   # io.puts(@@chat.size)    
	# Give the total number of people who are currently on chat.. 0 => 1 person on chat	

    # Leave a message on the chat queue to signify this client
    # has joined the chat
    @@chat << [my_client_id, ""]

   # io.puts(@@chat)    This contains all the client's id where transactions get started..






	loop do 
	      # Every 5 seconds check to see if we are receiving any data 
		#print "You all can start chatting now"
	      if IO.select([io], nil, nil, 15)
	        # If so, retrieve the data and process it..
	        line = io.readline
	      
		        # If the user says 'quit', disconnect them
	        	if line =~ /quit/
	        	  @@chat << [my_client_id, ""]
	        	  break
	        	end

	        # Shut down the server if we hear 'shutdown'
	        self.stop if line =~ /shutdown/
      
	        # Add the client's text to the chat array along with the
	        # client's ID
	        @@chat << [my_client_id, line]      
	      else
	        # No data, so print any new lines from the chat stream
	        @@chat[my_position..-1].each_with_index do |line, index|
	          io.puts("#{line[0]} says: #{line[1]}")
	      	end
        
        # Move the position to one past the end of the array
        my_position = @@chat.size
      end
    end


  end #loop ends

end # serve method ends

server = BasicServer.new(12345)
server.start
#server.serve(server)

server.join


#sleep 150
#server.shutdown
