require 'gserver'

class BasicServer < GServer

  def initialize(*args)
    super(*args)
    
    # Keep an overall record of the client IDs allocated
    # and the lines of chat
    @@client_id = 0
    @@chat = []
  end
  

  def serve(io)
  #  io.puts("Hello world!")
	  # Increment the client ID so each client gets a unique ID
    @@client_id += 1
    my_client_id = @@client_id
    my_position = @@chat.size
    
    io.puts("Welcome to the chat, client #{@@client_id}!\n")
	
   # io.puts(@@chat.size)    
	# Give the total number of people who are currently on chat.. 0 => 1 person on chat	

    # Leave a message on the chat queue to signify this client
    # has joined the chat
    @@chat << [my_client_id, ""]

   # io.puts(@@chat)    

  end
end

server = BasicServer.new(12345)
server.start
#server.serve(server)

sleep 60
server.shutdown
