#‘DD-MON-YYYY’, ‘mm-dd-yyyy’, ‘dd/mm/yy’ etc.”

puts ( Time.at(0) )

puts ( Time.utc(2010) )

puts Time.now
time = Time.new

puts time.year
puts time.month
puts time.day

print "\n Enter 1 for output in dd-mm-yyyy format\n Enter 2 for output in mm-dd-yyyy format \n Enter 3 for output in dd/mm/yy format \n Enter your choice: "

x = gets().to_i
case
	when x == 1 then puts("#{time.day}-#{time.month}-#{time.year}")
	when x == 2 then puts("#{time.month}-#{time.day}-#{time.year}")
	when x == 3 then puts("#{time.day}/#{time.month}/#{time.year%2000}")
end
