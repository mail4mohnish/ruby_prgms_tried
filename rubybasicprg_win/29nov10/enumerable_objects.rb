#Playing with Enumerable Objects
print("\nProgram Starts\n\n-------------------------------------------------------------------------- \n")

# Module 1
print("\nModule 1 Output\n\n-------------------------------------------------------------------- \n")


squares = [1,2,3].collect { |x| x*x }
print("Squares:")
puts(squares)
puts("\n")

#--------------------------------------------------------------------------------------------------------------------------------

# Module 2
print("\nModule 2 Output\n\n-------------------------------------------------------------------- \n")


print("Odd numbers upto range:\n")
odds = (1..25).select { |x| x%2 != 0 }
puts(odds)
puts("\n")

#--------------------------------------------------------------------------------------------------------------------------------

# Module 3
print("\nModule 3 Output\n\n-------------------------------------------------------------------- \n")

print("Even numbers upto range:\n")
evens = (1..25).reject { |x| x%2 != 0 }
puts(evens)
puts("\n")

#--------------------------------------------------------------------------------------------------------------------------------

# Module 4
print("\nModule 4 Output\n\n-------------------------------------------------------------------- \n")


#Check this out..its cool.. working of Inject..

data = [2,5,3,4]

sum = data.inject { | sum,x| sum + x } 
puts("\n#{sum} " ) 

max = data.inject { |m,x| m>x ? m : x }

puts(max)

#it work like this.. the first elemnent is taken as the "accumulated" value and the next value is the one which is taken as the "iterated" value..
#based on the condition.. whatever is satisfied..the computed value becomes the next "accumulated" value and by default the iterator picks up the next value as the "iterated" value...

print("\nProgram Ends\n\n---------------------------------------------------------------------------- \n\n")
