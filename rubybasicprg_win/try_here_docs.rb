#playin with string.. especially "Here Docs" which are special ways to generate strings..taken frm perl etc.
print("\nProgram Starts\n\n-------------------------------------------------------------------------- \n")

# Module 1
print("\nModule 1 Output\n\n-------------------------------------------------------------------- \n")


puts(%q(Don't worry about escaping ' characters!))
puts(%Q{\n\tSomebody said: "You look good today:)!!" \n})


# Module 2
print("\nModule 2 Output\n\n-------------------------------------------------------------------- \n")

my_string = <<MY_STRING
This is a simple string that is
pre-formatted, which means that the
way it is formatted here including
tabs and newlines will be duplicated
when I print it out.
MY_STRING

puts(my_string)



# Module 3
print("\nModule 3 Output\n\n-------------------------------------------------------------------- \n")

hdoc2 = <<EODOC 
I wandered lonely as a cloud, That floats on high o'er vale and hill...
hi.. whts up..!!
EODOC

puts("EODOC")

puts('EODOC')

print("#{hdoc2}")

print("\nProgram Ends\n\n---------------------------------------------------------------------------- \n\n")
