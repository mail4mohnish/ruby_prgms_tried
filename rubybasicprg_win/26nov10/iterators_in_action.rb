
# working with iterators
print("\nProgram Starts\n\n-------------------------------------------------------------------------- \n")

# Module 1
print("\nModule 1 Output\n\n-------------------------------------------------------------------- \n")


3.times { puts "thank you!" }

puts("\n")


#--------------------------------------------------------------------------------------------------------------------------------------------

# Module 2
print("\nModule 2 Output\n\n-------------------------------------------------------------------- \n")


data = [1,2,3,4,5]

data.each {|x| puts x}

# note how x although its not being declared or defined.. works for each element of the array data..




#--------------------------------------------------------------------------------------------------------------------------------------------

# Module 3
print("\nModule 3 Output\n\n-------------------------------------------------------------------- \n")


puts("\n")

[1,2,3,4,5].map { |x| puts x*x }



#--------------------------------------------------------------------------------------------------------------------------------------------

# Module 4
print("\nModule 4 Output\n\n-------------------------------------------------------------------- \n")


# see the beauty of iterators in Ruby in terms of how the factorial is implemented...


print("Enter the number whose factorial you want to find out: ")
n = gets.to_i 
#note the change made in syntax of gets to input an integer..


$factorial = 1 # also do 
2.upto(n) { |x| $factorial *= x }
# also do note the use of the global factorial variable


puts("Factorial of #{n} is:- #{$factorial} ")

print("\nProgram Ends\n\n---------------------------------------------------------------------------- \n\n")
