# playing with numbers.. Checking the general backslashes to be run..
print("\nProgram Starts\n\n-------------------------------------------------------------------------- \n")
# Module 1
print("\nModule 1 Output\n\n-------------------------------------------------------------------- \n")

puts( 4 * 5)
puts("\n")
puts (100/20 + 90/54) 
puts("\t") 
puts(20 + 30)


# Module 2
print("\nModule 2 Output\n\n-------------------------------------------------------------------- \n")

# Escape sequences and all need to be given with double quotes not single quotes.. puts("\n")
puts("\n")
num = 50 - 30
puts("Subtraction of 50 - 30 is: \t #{num}")
num1 = 3**3
puts("Exponentiation of 3^3 is: \t #{num1}")


# Module 3
print("\nModule 3 Output\n\n-------------------------------------------------------------------- \n")

# Modulo in operation.. do note..
num2 = 5%3 
puts("Value of 5 modulo 3 is: \t #{num2}")
puts('\n')

print("\nProgram Ends\n\n---------------------------------------------------------------------------- \n\n")
