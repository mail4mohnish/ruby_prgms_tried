def check_viscosity		
	begin
		#rnum = ( diameter * velocity * density )/viscosity
		raise "Divide by Zero exception. Please re-enter the viscosity" unless $viscosity != 0
	rescue Exception => e
		puts e.message
		print("Please Renter the viscosity: ")
		$viscosity = gets().to_i
		if $viscosity == 0
			check_viscosity
		end
	end
end

def calculate_reynolds_number
	print("Enter the diameter: ")
	diameter = gets().to_i
	print("Enter the velocity: ")
	velocity = gets().to_i
	print("Enter the density: ")
	density = gets().to_i
	print("Enter the viscosity: ")
	$viscosity = gets().to_i

	check_viscosity
	#while(viscosity == 0)

	#end
		rnum = ( diameter * velocity * density )/$viscosity
	return rnum

end

# instance variables like @viscosity could also be used..
# Normally global variables (the ones with $ symbol) are used for enviromnment definitions..

ch = 'y'

if ( ch == 'y' )

	begin

		reynolds_number = calculate_reynolds_number

		puts "Reynolds number is: #{reynolds_number} "

		if (reynolds_number < 2100)
			puts 'Laminar flow'
		elsif reynolds_number >= 2100 && reynolds_number <= 4000
			puts 'Transient flow'
		else
			puts 'Turbulent Flow'
		end

		print "do you want to continue(y/n)?"
		ch = gets.chomp

	end while not ch != 'y'

else
print("The entered character is not y meaning you don't want to continue")
end


