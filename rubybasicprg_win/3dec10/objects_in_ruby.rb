# Working of Objects in ruby.. By the way..:) Everything in Ruby is actually an object....
print("\nProgram Starts\n\n-------------------------------------------------------------------------- \n")
# Module 1
print("\n\n----------------------------------\n\nObject References - Module 1 Output\n\n----------------------------------\n\n")

s = "Ruby"
# a reference to an object is copied into the a variable s
t = s
# the same reference is copied to t
puts(s)
t[-1] = ""
#note since its a reference a change in t will reflect in s too..
t = "Check"
puts s,t


# Module 2
print("\n\n----------------------------------\n\nObject Class and Type. Append Operator. - Module 2 Output\n\n----------------------------------\n\n")

o = "test"
k = o.class
puts("Class of \"test\" is: #{k}")
l = k.superclass
puts("Super Class of \"test\" is: #{l}")
m = l.superclass
puts("Super Class of Object Class is: #{m}")


#their use can be as:-
a = "Will"
print("The class of \"Will\" is string? (true/false): #{a.class == String}\n")
print("\"Will\" is an instance of a string? (true/false): #{a.instance_of? String}\n")
b = " Smith"


x = 1
x.instance_of? Fixnum
x.instance_of? Numeric
x.is_a? Integer
x.is_a? Comparable

puts(Numeric === x)


#Note the use of the append operator...
c = a<<b

puts(c)
puts c.respond_to? :"<<" and not o.is_a? Numeric 
# it evaluate to true if c has an Append operator and if the object o is not numeric.



# Module 3

print("\n\n----------------------------------\n\nObject equality - Module 3 Output\n\n----------------------------------\n\n")

# equal? method and == operator 


a = "ruby"

b = c = "ruby"

puts("Does a and b refer to the same object?(true/false): #{a.equal?(b)}\n")

puts("Do a and b contain the same value?(true/false): #{ a == b }\n")

puts("Does b and c refer to the same object?(true/false): #{b.equal?(c)}\n")

# an interesting thing to note here is that this works


# eql? method

puts(1 == 1.0)

puts(1.eql?(1.0))


# Module 4

print("\n\n----------------------------------\n\nObject Order - Module 4 Output\n\n----------------------------------\n\n")

# the comparison operator

puts(1 <=> 5) 
# if left operand is less than the right operand.. the operator returns -1
puts(5 <=> 5)
# if left operand is equal to the right operand.. the operator returns 0
puts(6 <=> 5)
# if left operand is greater than the right operand.. the operator returns 1
puts("1" <=> 5)


# working of between?

#puts(3.between?(1..20))

puts(5.between?(1,4))

# Module 5

print("\n\n----------------------------------\n\nFreezing Objects in Ruby - Module 5 Output\n\n----------------------------------\n\n")

# The funda of constants in Ruby using freeze and frozen?

s = "ice"
d = "try"
puts(s[0])
s[0] = "ni"
# see the flexibility in ruby.. here you need not have any index of an array having only one character like we have in C/C++ programming for e.g.
puts("\nBefore freezing: #{s}\n")

s.freeze

puts("Is s frozen(true/false)?: #{s.frozen?}")

puts(d.upcase)
puts(d)
print("Cool thing: check out the difference between d.upcase and d.upcase! refer the above and below 2 versions of string \"try\"\n")
#s.upcase! if you uncomment this it would give a type error saying that you can't modify frozen strings..
puts(d.upcase!)
puts(d)
#note syntax by which strings are converted to upper case in ruby
puts(s)


print("\nProgram Ends\n\n---------------------------------------------------------------------------- \n\n")






