

#checking variables wrt default values.. Local and Class variables(@@t for e.g. ) will give NameError

print("\nProgram Starts\n\n-------------------------------------------------------------------------- \n")

# Module 1
print("\nModule 1 Output\n\n-------------------------------------------------------------------- \n")


puts("\n")


@b

puts("\nInstance variable if has no value assigned.. by default has a nil value printed with it")
puts "default value of instance variable b is: #{@b} "


# Module 2
print("\nModule 2 Output\n\n-------------------------------------------------------------------- \n")


$c
puts("\nGlobal variable if has no value assigned.. by default has a nil value printed with it")
puts "default value of global variable c is: #{$c} "

print("\nProgram Ends\n\n---------------------------------------------------------------------------- \n\n")
