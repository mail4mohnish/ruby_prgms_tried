print("\nProgram Starts\n\n-------------------------------------------------------------------------- \n")

# Module 1
print("\nModule 1 Output\n\n-------------------------------------------------------------------- \n")


# Equality operators( '==' and check this '==='( case equality operator.. do note.. ) ).

a1 = 3
a2 = 4
a3 = 0

s1 = (a1==a2)
puts(s1)

s2 = (a3!=a2)
puts(s2)

z1 = [ 4..10 ]

puts((1..10) === 5)

#--------------------------------------------------------------------------------------------------------------------------------

# Module 2
print("\nModule 2 Output\n\n-------------------------------------------------------------------- \n")

#Boolean operators( &&, ||, ! ), new things to learn here.. do note..

b1 = a3 && a2
# NOTE THIS!! here what happens is that the boolean && in Ruby works differently.. if the 1st operand is neither nil or false.. 
puts(b1)
# the value of the second operand is printed as the output.. Advtg of using this is its like a short-circuit operator..

b2 = a3 & a1
#this is the logical AND.. do note the difference..
puts(b2)
# Advtg of using this is.. its like a short-circuit operator..


b3 = a1 || a2
# note this too.. here.. if the 1st operand is neither a nil or false the op is the 1st operand.. else its second operand..
puts(b3)

b4 = 0 | a3
#this is the logical OR .. do note the difference..
puts(b4)

b5 = !a3
# NOTE THIS!! in Ruby 0 is not equivalent to nil or false.. as you would otherwise find in C Programming.. hence the op
puts(b5)

b6 = !nil
puts(b6)

b7 = !false
puts(b7)

#--------------------------------------------------------------------------------------------------------------------------------

# Module 3
print("\nModule 3 Output\n\n-------------------------------------------------------------------- \n")

#cool..check this out..!! + conditional operator.. + defined?

c = 4
c += 3
puts(c)


c1,c2,c3 = 4,5,6
#cool.......!!
puts(c1)
puts(c2)
puts(c3)

c4=c5=7
puts(c4)
puts(c5)


n = 2
puts("You have #{n} #{n==1 ? 'message' : 'messages'}")



c3>c5 ? c3=9 : c5=9
# do note if its given the way below.. there is an error.. probably as it would take :c5 as a symbol n not a part of the conditional op.
#c3>c5?c3=9:c5=9
puts(c3)
puts(c5)


s1 = c if defined?
c # if variable is defined copy its value to s1..
puts(s1)

print("\nProgram Ends\n\n---------------------------------------------------------------------------- \n\n")
#--------------------------------------------------------------------------------------------------------------------------------
