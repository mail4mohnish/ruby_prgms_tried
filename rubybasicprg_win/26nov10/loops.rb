# loops in Ruby

print("\nProgram Starts\n\n-------------------------------------------------------------------------- \n")

# Module 1
print("\nModule 1 Output\n\n-------------------------------------------------------------------- \n")


# While

x = 10
while x >= 0 do
	puts x
	x = x - 1
end


#--------------------------------------------------------------------------------------------------------------------------------

# Module 2
print("\nModule 2 Output\n\n-------------------------------------------------------------------- \n")


puts("\n")



# Until

x = 0
until x > 10 do
	puts x
	x = x + 1
end


#--------------------------------------------------------------------------------------------------------------------------------

# Module 3
print("\nModule 3 Output\n\n-------------------------------------------------------------------- \n")

puts("\n")



# While as a modifier

x = 5
puts x = x + 1 while x < 10


#--------------------------------------------------------------------------------------------------------------------------------

# Module 4
print("\nModule 4 Output\n\n-------------------------------------------------------------------- \n")

puts("\n")



# Until as a modifier

a = [1,2,3]

puts a.pop until a.empty? # behaves like a stack.. the op is in LIFO manner


#--------------------------------------------------------------------------------------------------------------------------------

# Module 5
print("\nModule 5 Output\n\n-------------------------------------------------------------------- \n")


#for loop implementation..


puts("\n")

array = [ 1, 2, 3, 4, 5 ]

for element in array 
	puts element 
end

# note the use of built in keywords like element simplifies our usage..
# making ruby more simpler to use


puts("\n")

hash = { :a => 1, :b => 2, :c => 3 } 

# here key and value are built in keywords wrt hashes in Ruby..

for key,value in hash
	puts "#{key} => #{value}"
end

print("\nProgram Ends\n\n---------------------------------------------------------------------------- \n\n")
#--------------------------------------------------------------------------------------------------------------------------------
