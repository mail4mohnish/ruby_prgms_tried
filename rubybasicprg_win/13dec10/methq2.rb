

def calculate_reynolds_number
	print("Enter the diameter: ")
	diameter = gets().to_i
	print("Enter the velocity: ")
	velocity = gets().to_i
	print("Enter the density: ")
	density = gets().to_i
	print("Enter the viscosity: ")
	viscosity = gets().to_i
	rnum = ( diameter * velocity * density )/viscosity
	return rnum
end


reynolds_number = calculate_reynolds_number

puts "Reynolds number is: #{reynolds_number} "

if (reynolds_number < 2100)
	puts 'Laminar flow'
elsif reynolds_number >= 2100 && reynolds_number <= 4000
	puts 'Transient flow'
else
	puts 'Turbulent Flow'
end

	
