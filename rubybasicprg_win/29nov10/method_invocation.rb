#Method invocation.. the concept is pretty much the same as in other OOP Languages...
print("\nProgram Starts\n\n-------------------------------------------------------------------------- \n")


#--------------------------------------------------------------------------------------------------------------------------------

# Module 1
print("\nModule 1 Output\n\n-------------------------------------------------------------------- \n")


a = [1,2,3,4,5]

a.each{ |x| puts x }

puts("\n")


#--------------------------------------------------------------------------------------------------------------------------------

# Module 2
print("\nModule 2 Output\n\n-------------------------------------------------------------------- \n")


message = "Hey.. I think I have seen you somewhere..!?!"
puts(message::length)
# do note.. method.length also works.. the idea behind not using " :: " is that it is more like constant reference expressions...


#--------------------------------------------------------------------------------------------------------------------------------

# Module 3
print("\nModule 3 Output\n\n-------------------------------------------------------------------- \n")


puts("\n")

#Puts is also a method... a kernel method... this also is an e.g. of method invocation..
puts "Hey check this out.."

print("\nProgram Ends\n\n---------------------------------------------------------------------------- \n\n")

