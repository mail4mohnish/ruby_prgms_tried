# Working with Procs & Lambdas
print("\nProgram Starts\n\n-------------------------------------------------------------------------- \n")

# Module 1

print("\n\n----------------------------------\n\nCreating Procs(procs & lambdas) - Module 1 Output\n\n----------------------------------\n\n")

def makeproc(&p)
	p
end
# This method returns a Proc object
adder = makeproc { |x,y| x+y }
# Procs have a block like behaviour
sum = adder.call(2,2)
puts sum
# The above is one way to create a proc


a,b = [1,2,3], [4,5]
summation = Proc.new { |total,x| total + x }
sum1 = a.inject(0, &summation)
sum2 = b.inject(sum1, &summation)
puts sum1
puts sum2
# The above code is another way to create a a procedure using Proc.new
# The above code also shows a way through which we can '&' to invoke a function.. summation is the object..which invokes a function..

p = Proc.new { |x,y| x+y }
# The proc method returns a Proc object which is a proc
# This way we make p an object to the procedure. 


#invoke
a1 = p.call(2,3)
# the object p invokes the procedure and returns the computed value and stored in a1
puts a1


#Kernel.lambda

is_positive = lambda { |x|  puts "Positive" if x > 0}
# The lambda method returns a Proc object which is a lambda..


is_positive.call 4
#puts(is_positive)


#Lambda as a literal, these notations are followed from Ruby 1.9 onwards
succ = ->(x) { puts x + 1 }
succ.call(2)
#zoom = -> x,y

def compose(f,g)
 ->(x) { f.call(g.call(x)) }
end

succOfSquare = compose(->x{x+1}, ->x{x*x})
succOfSquare.call(4)
# we have g.call(x) first and then outside we have f.call(x); thus we have multiplication computation followed by the addition
# This is a really cool thing in Ruby.. do note..


# Module 2

print("\n\n----------------------------------\n\nProcs & Lambdas(difference between them) - Module 2 Output\n\n----------------------------------\n\n")

# Wrt return statement
def whowouldwin

  mylambda = lambda {return "Freddy"}
  mylambda.call

  # mylambda gets called and returns "Freddy", and execution
  # continues on the next line

  return "Jason"

end


a1 = whowouldwin
print "using lambdas the output would be: "
puts a1
#=> "Jason"


def whowouldwin2

  myproc = Proc.new {return "Freddy"}
  myproc.call

  # myproc gets called and returns "Freddy", 
  # but also returns control from whowhouldwin2!
  # The line below *never* gets executed.

  return "Jason"

end
#remember and please note that the procedure is behaving like a block and hence a similar nature of return is observed

a2 = whowouldwin2         
print "using procs the output would be: "
puts a2
#=> "Freddy"


#difference wrt break..

def test
	puts "entering test method"
	proc = Proc.new { puts "entering proc"; break }
	#proc.call
	puts "exiting test method"
end
test


def test1
	puts "entering test method"
	lambda = lambda { puts "entering lambda"; break; puts "exiting lambda" }
	lambda.call
	puts "exiting test method"
end
test1
#here again do note lambdas are method like so breaking from a method won't be applicable as we generally break through loops and similar things.


#difference wrt method arguments

p1 = Proc.new { |x,y| print x,y }
p1.call(1,2)
p1.call(1,2,3)

l1 = lambda { |x,y| print x,y }
l1.call(1,2)
l1.call(*[1,2])


# Module 3

print("\n\n----------------------------------\n\nInvoking Procs and Lambdas, The Arity of a Proc - Module 3 Output\n\n----------------------------------\n\n")
#
x = 5
y = 6
f = Proc.new { |x,y| 1.0/(1.0/x + 1.0/y) }

z1 = f.call(1,2)
puts z1

z2 = f[2,3]
puts z2

z3 = f.(3,4)
puts z3

z4 = f.(x,y)
puts z4
#the above are the different ways of calling the proc wrt the proc object f

is_even = lambda { |x|  puts "Even" if x % 2 == 0}
# The lambda method returns a Proc object which is a lambda..


y1 = is_even[4]
#puts(is_even)
puts y1

y2 = is_even[4]
puts y2

y3 = is_even.(4)
puts y3

y4 = is_even.(x)
puts y4
#return a nill as no string is returned for an odd number.


#Arity

puts lambda{ |x| x }.arity

puts f.arity

print("\nProgram Ends\n\n-------------------------------------------------------------------------- \n")



