#Modify the above program such that it will ask for ‘Do you want to calculate again (y/n), if you say ‘y’, it’ll again ask the parameters. If ‘n’, it’ll exit. (Do while loop) While running the program give value mu = 0. See what happens. Does it give ‘DIVIDE BY ZERO’ error? Does it give ‘Segmentation fault..core dump?’. How to handle this situation. Is there something built in the language itself? (Exception Handling)



def calculate_reynolds_number
	print("Enter the diameter: ")
	diameter = gets().to_i
	print("Enter the velocity: ")
	velocity = gets().to_i
	print("Enter the density: ")
	density = gets().to_i
	print("Enter the viscosity: ")
	viscosity = gets().to_i
	
	
	rnum = ( diameter * velocity * density )/viscosity
	return rnum
end






ch = 'y'

if ( ch == 'y' )

	begin

		reynolds_number = calculate_reynolds_number

		puts "Reynolds number is: #{reynolds_number} "

		if (reynolds_number < 2100)
			puts 'Laminar flow'
		elsif reynolds_number >= 2100 && reynolds_number <= 4000
			puts 'Transient flow'
		else
			puts 'Turbulent Flow'
		end

		print "do you want to continue(y/n)?"
		ch = gets.chomp

	end while not ch != 'y'

else
print("The entered character is not y meaning you don't want to continue")
end


