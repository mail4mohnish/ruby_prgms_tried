# Working with Methods 
print("\nProgram Starts\n\n-------------------------------------------------------------------------- \n")

# Module 1

print("\n\n----------------------------------\n\nDefining Singleton Methods, Undefining Methods - Module 1 Output\n\n----------------------------------\n\n")

o = "message"
def o.printme
	puts self
end
o.printme

#undef o.printme

a = 4
b = 5

def swap(a,b)
	a = a*b
	b = a/b
	a = a/b
	return a,b
end
puts swap(a,b)
undef swap

#puts swap(a,b)
#the above code line will give you an err saying undefined method 'swap'

# Module 2

print("\n\n----------------------------------\n\nMethod Aliases, Required Parentheses - Module 2 Output\n\n----------------------------------\n\n")

def hello
	puts "Hello World"
end

alias original_hello hello

def hello
	puts "Your attention please"
	original_hello
	puts "This has been a test"
end

#x = 1
@y = 2
#note how easily we can make use of instance variables over here..

def sum x
	total = x + @y
	return total
end

puts (sum 2),2

#do note the confusion arises and moreover an error occurs when we use puts (sum 2,2), here its not clear whether 2 arguments are being passed to the method sum like puts ( sum(2,2) ) or actually are we trying something like puts ( sum(2), 2 )
#or are we trying to print 2 along with the return value from the sum method. Thus here we require parentheses here.. imp. do note..


# Module 3
print("\n\n----------------------------------\n\nMethod Arguments - Module 3 Output\n\n----------------------------------\n\n")

# Module 3 a

print("\n\n----------------------------------\n\nParameter Defaults, Variable-Length Argument Lists - Module 3a Output\n\n----------------------------------\n\n")

# Using Parameter Defaults
def prefix( s, len = 1 )
	puts(s[0, len])
	puts("\n")
end

prefix("Cognizant")
prefix("Cognizant",5)

def suffix( s, index = s.size-1 )
	puts("Index is: #{index}")
	puts("Size of string is: #{s.size}")
	puts(s[index, s.size-index])
	puts("\n")
end

suffix("Cognizant")
suffix("Cognizant", 5)

a = [ "Hi" ]
def append(x,a=[])
	a<<x
	puts a 
#the color scheme changes with a ruby editor.. as and when we make use of the append(<<) operator in ruby..
end

append(" Friend", a)


append("Yapdi irika")
#Creates a new array if previously no array existed

# the below method finds the greatest among elements passed to it....
def max(first, *rest)
	max = first
	rest.each { |x| max = x if x > max }
	return max
end
puts("\n")
puts("the greatest element among the ones passed is: #{max(1,2,3)}") # first = 1, rest = [2,3]
# Pretty cool thing in Ruby.. do note..
puts("\n")
data = [1,2,3,4,5]
m1 = max(data)
puts m1
#passes the entire array to the 'first' argument of thee max method..
puts("\n")
m2 = max(*data)
puts m2

# Module 3 b

print("\n\n----------------------------------\n\nHashes for Named Arguments, Block Arguments - Module 3b Output\n\n----------------------------------\n\n")


def sequence(args)

	n = args[:n] || 0 # this represents the total no. of elements in the array..
	m = args[:m] || 1
	c = args[:c] || 0
	
	a = []

	n.times { |i| a << m*i+c }
	return a
end


m3 = sequence :n => 3 , :m => 5
puts("#{m3}\n")

m4 = sequence c:1, m:3, n:5
puts("#{m4}\n\n")



def sequence3(n,m,c, &b)
	i = 0
	while(i<n)
		b.call(i*m + c)
		i += 1
	end
end

sequence3(5,2,2) { |x| puts x }



