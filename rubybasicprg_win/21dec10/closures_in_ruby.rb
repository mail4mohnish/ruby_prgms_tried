# Working with Closures
print("\nProgram Starts\n\n-------------------------------------------------------------------------- \n")

# Module 1
print("\n\n----------------------------------\n\nClosures, Closures with Shared variables - Module 1 Output\n\n----------------------------------\n\n")

def multiplier(n)
	lambda { |data| data.collect { |x| x*n } }
end

doubler = multiplier(2)
# the most important thing to note here is that the value of n is bound and somewhat kept with the multiplier method
puts doubler.call([1,2,3])

print "\n"

def accessor_pair(initialValue = nil)
	value = initialValue
	getter = lambda { value }
	setter = lambda { |x| value = x }
	return getter,setter
end

getX, setX = accessor_pair(0)
puts getX[]
setX[10]
puts getX[]

print "\n"

def multipliers(*args)
	x = nil
	args.map{ |x| lambda { |y| x*y } }
end

double, triple = multipliers(5,3)
# x a local variable the entire variable is retained by the closure. This is an interesting concept of closures.
puts double.call(3)

# Module 2
print("\n\n----------------------------------\n\nClosures and Bindings - Module 2 Output\n\n----------------------------------\n\n")

def multiplier(n)
	lambda { |data| data.collect{ |x| x*n } }
end

doubler = multiplier(2)

puts doubler.call([1,2,3])

print "\n"

doubler.binding.eval("n=3")


puts doubler.call([1,2,3])

print("\nProgram Ends\n\n-------------------------------------------------------------------------- \n")
