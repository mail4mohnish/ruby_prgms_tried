# Working with Classes
print("\nProgram Starts\n\n-------------------------------------------------------------------------- \n")

# Module 1
print("\n\n----------------------------------\n\nClasses in Ruby(Basics) - Module 1 Output\n\n----------------------------------\n\n")


class Point

	x = 2
	y = 3
	p = Point.new
	puts p.class
	puts p.is_a? Point
	@x, @y = x,y	
	
end

class Point2
	
	def initialize(x,y)
	@x, @y = x,y
	end

	
end


p2 = Point2.new(1,2)

#

class Point3

	def initialize(x,y)
		@x, @y = x,y
	end

	def x
		@x
	end

	def y
		@y
	end

	def x=(value)
		@x = value
	end

	def y=(value)
		@y = value
	end

end

p3 = Point3.new(1,2)
p4 = Point3.new(p3.x*2, p3.y*3) 
p5 = Point3.new(1,1)

puts p3
puts p4
puts p5
#p.x = 0
#p.y = 0




