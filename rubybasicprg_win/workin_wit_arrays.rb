# working wit arrays - covering basic examples .. not all..
print("\nProgram Starts\n\n-------------------------------------------------------------------------- \n")

#--------------------------------------------------------------------------------------------------------------------------------


# Module 1
print("\nModule 1 Output\n\n-------------------------------------------------------------------- \n")

a = [ 1,3,5,7,9,11 ]

# prints the 1st element value of the array...
puts(a[0]) 

#--------------------------------------------------------------------------------------------------------------------------------

# Module 2
print("\nModule 2 Output\n\n-------------------------------------------------------------------- \n")

# prints the length of the array...
print( a.size ) 

puts("\nUsing size(above) n length(below) properties to print no. of elements in a string\n")

print( a.length )

#--------------------------------------------------------------------------------------------------------------------------------

# Module 3
print("\nModule 3 Output\n\n-------------------------------------------------------------------- \n")

# doesn't work..but the below will work..

print(a[8]) 


#--------------------------------------------------------------------------------------------------------------------------------

# Module 4
print("\nModule 4 Output\n\n-------------------------------------------------------------------- \n")

print("\ntryin to print anything outside array bounds ..like value of a[8] prints nil.. do note.. ")

puts(a[8])

puts(a[-1])

#--------------------------------------------------------------------------------------------------------------------------------

# Module 5
print("\nModule 5 Output\n\n-------------------------------------------------------------------- \n")

print(" note how through a[-3] we get the value of the third last element of the array ")
puts(a[-3])


print("changing value of 1st element of array to 25.. and printing it.. note the way its printed..")
a[0] = 25
puts(a[-a.size])


print("Dyanmically do note how the size of an array can be increased in Ruby..Making things simpler n Faster..\
and a programmer can't be happier.. Yukihiro Matsumto\n")

a[6] = "zero"
puts("value of a[6]:\t #{a[6]} ")
print("Also note how ruby is allowing the programmer to have an array with diverse data types... \n")


a[9] = (26..30).to_a


print("\nto print the elements within the range of a[9]\n")
puts(a[9,]) 

#--------------------------------------------------------------------------------------------------------------------------------

# Module 6
print("\nModule 6 Output\n\n-------------------------------------------------------------------- \n")

print("\nto print the first two elements of an array\n")

puts(a[0..1]) 


#--------------------------------------------------------------------------------------------------------------------------------

# Module 7
print("\nModule 7 Output\n\n-------------------------------------------------------------------- \n")

puts("\nValues of array before change within a specified range..: \n #{a[1..9]} ")
print("\nchanging values of an array within a specified range")
a[3..4] = ['Yapdi','Irika']

#--------------------------------------------------------------------------------------------------------------------------------

# Module 8
print("\nModule 8 Output\n\n-------------------------------------------------------------------- \n")

puts("\n #{a[2..5]} ")

print("\nNote the difference between the above op and the op below due to the diff of just one '.' in the range 2 to 5 \n")

puts("\n #{a[2...5]} ")

#--------------------------------------------------------------------------------------------------------------------------------

# Module 9
print("\nModule 9 Output\n\n-------------------------------------------------------------------- \n")


# now dealing mainly with ranges, true, false and nil..

r = 'a'..'c'

puts(r.include? 'a')
puts(r.member? 'b') 
#member and include are like synonyms.. do note..

puts(r.cover? 'c') 
# even the cover works the same.. as member n include..

print("does the array of r include the symbol l?: ")
puts(r.include? 'l')
print("\na[7] is nil? ans: ")
puts(a[7].nil?)

# Module 10
print("\nModule 10 Output\n\n-------------------------------------------------------------------- \n")

#working of self( it functions like the this pointer C++ ).. But Do note...the pointer concept is not there in Ruby

o = "message"

#Defining a  method called printme in Ruby 
def o.printme
	puts self
end

o.printme

print("\nProgram Ends\n\n---------------------------------------------------------------------------- \n\n")
#--------------------------------------------------------------------------------------------------------------------------------
