print("\nProgram Starts\n\n-------------------------------------------------------------------------- \n")

# Module 1
print("\nModule 1 Output\n\n-------------------------------------------------------------------- \n")

# chck the working of single quoted strings, the working of backslashes..
 
puts('Hi.. aren\'t you the same person I met day before yesterday?') 
puts('this string literal ends with a backslash: \\')
puts('\n')
puts(' thus you can see that all escape sequences are not supported by single quotes ')
 
#--------------------------------------------------------------------------------------------------------------------------------

# Module 2
print("\nModule 2 Output\n\n-------------------------------------------------------------------- \n")
 
puts('this is a backslash quote: \\\'')
puts('two backslashes: \\\\')
 
#--------------------------------------------------------------------------------------------------------------------------------

# Module 3
print("\nModule 3 Output\n\n-------------------------------------------------------------------- \n")

#puts("\n Note how multiple lines are generated in user defined messages using back slash with Single quotes: ")

message = 'These three literals are \ 
concatenated into one by the interpreter.\ 
The resulting string contains no newlines.'
print("The message is: \n")

puts(message)
 
#--------------------------------------------------------------------------------------------------------------------------------

# Module 4
print("\nModule 4 Output\n\n-------------------------------------------------------------------- \n")

# working with double quoted strings.. its used over single quotes.. when we want to utilise escape sequences
 
puts("\t\"This quote begins with tab n ends with a newline\"\n")

#Note the role played by the back slashes in printing the quotes in the op console
#puts("printing a single backslash using double quotes: \n")

puts("\\") # prints a single backslash...
 
#puts("\n Note how multiple lines are generated in user defined using back slash with Doublequotes: ")

puts("This string literal 
has two lines \
but is written on three")

print("\nProgram Ends\n\n---------------------------------------------------------------------------- \n\n")
