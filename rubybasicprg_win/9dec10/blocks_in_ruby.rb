# Working with Blocks in ruby.. 
print("\nProgram Starts\n\n-------------------------------------------------------------------------- \n")

# Module 1

print("\n\n----------------------------------\n\nBlock Syntax,Simple operations with Blocks - Module 1 Output\n\n----------------------------------\n\n")

1.upto(10) { |x| puts x }
puts("\n")
1.upto(10) do |x|
	puts x
end

1.upto(3) { |x| puts x }
#1.upto(10)
#{ |x| puts x }

week = Hash.new
week = { :sunday => 1, :monday => 2, :"tuesday" => 3, :wednesday => 4, :thursday => 5, :'friday' => 6, :saturday => 7 }

week.each do | key, value |
	puts "#{key}: #{value}"
end
puts("\n")

# Module 2

print("\n\n----------------------------------\n\nInvoking a Block - Module 2 Output\n\n----------------------------------\n\n")

aBlock = lambda { |x| puts x }
aBlock.call "Hello World!"
#lambda is kernel method.
#aBlock =  { |x| puts x }
#the above way of defining a block gives a syntax error
#Do note blocks have only valid syntax when they are an argument to a method call.

# Module 3

print("\n\n----------------------------------\n\nReturning from a Block - Module 3 Output\n\n----------------------------------\n\n")

words = Hash.new
words = { :Thanks => 1, :Smile =>2 }

puts(words.sort)
puts("\n#{words}\n")

array1 = [ 2,3,0,4,5 ]

array1 = array1.collect do|x|
		next 0 if x == nil
		next x, x*x
	 end
puts(array1)



# Module 4

print("\n\n----------------------------------\n\nWriting a method that accepts a Block - Module 4 Output\n\n----------------------------------\n\n")

def test
   yield 5
   puts "You are in the method test"
   yield 100
end
test {|i| puts "You are in the block #{i}"}


# Module 5

print("\n\n----------------------------------\n\nBlocks and Variable Scope - Module 5 Output\n\n----------------------------------\n\n")

x = y = 0
1.upto(4) do |x;y|
	y = x + 1
	puts y*y
end

puts([x,y])


print("\nProgram Ends\n\n---------------------------------------------------------------------------- \n\n")


