# Working with Exceptions in ruby.. 
print("\nProgram Starts\n\n-------------------------------------------------------------------------- \n")
# Module 1

print("\n\n----------------------------------\n\nRaising an exception - Module 1 Output\n\n----------------------------------\n\n")

def inverse(x)
	raise "Argument is not numeric"
	return	1.0/x
end

y = inverse(2)
puts(y)
